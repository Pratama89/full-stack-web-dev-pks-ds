<?php
// require_once 'Hewan.php';
// require_once 'Fight.php';

spl_autoload_register(function ($class_name) {
  include $class_name . '.php';
});

class Elang extends Hewan
{
  public function get_nama()
  {
    return 'Nama Hewannya: ' . $this->nama;
  }

  public function get_keahlian()
  {
    return 'Keahliannya adalah: ' . $this->keahlian;
  }

  public function get_jumlahKaki()
  {
    return 'Jumlah kaki Elang ...' . $this->jumlahKaki;
  }

  public function get_attackPower()
  {
    return 'Kekuatannya adalah: ' . $this->attackPower;
  }

  public function get_defencePower()
  {
    return 'Kelemahanya adalah: ' . $this->defencePower;
  }
}
