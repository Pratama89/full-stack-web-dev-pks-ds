<?php
spl_autoload_register(function ($class_name) {
  include $class_name . '.php';
});

class Harimau extends Hewan
{
  public function get_nama()
  {
    return 'Nama Hewan ini: ' . $this->nama;
  }

  public function get_keahlian()
  {
    return 'Keahliannya : ' . $this->keahlian;
  }

  public function get_jumlahKaki()
  {
    return 'Jumlah kaki: ' . $this->jumlahKaki;
  }

  public function get_attackPower()
  {
    return 'Kekuatannya : ' . $this->attackPower;
  }

  public function get_defencePower()
  {
    return 'Kelemahanya : ' . $this->defencePower;
  }
}
