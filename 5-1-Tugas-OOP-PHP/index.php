<?php
spl_autoload_register(function ($class_name) {
  include $class_name . '.php';
});


$hewan1 = new Hewan('Kucing', 60, '4', 'berlari');
$hewan1->set_darah(70);
echo 'Nama Hewan: ' . $hewan1->get_nama() . '<br>' . 'Jumlah Darah: ' . $hewan1->get_darah() . '<br>' . 'Jumlah kaki ada: ' . $hewan1->get_jumlahKaki() . '<br>' . 'Keahliannya: ' . $hewan1->get_keahlian() . '<br>' . '<br>';

$elang = new Elang('Elang', 50, '2', 'terbang tinggi');
$elang2 = new Fight(10, 5);
echo $elang->get_nama() . '<br>' . $elang->get_keahlian() . '<br>' . $elang->get_jumlahKaki() . '<br>' . '<br>' . 'Kekuatannya adalahh: ' . $elang2->get_attackPower() . '<br>' . 'Kelemahannya adalahh: ' . $elang2->get_defencePower() . '<br>' . '<br>';

$harimau = new Harimau('Harimau', 7, '8', 'Lari Cepat');
$harimau2 = new Fight(7, 8);
echo $harimau->get_nama() . '<br>' . $harimau->get_keahlian() . '<br>' . $harimau->get_jumlahKaki() . '<br>' . '<br>' . 'Kekuatannya adalahh: ' . $harimau2->get_attackPower() . '<br>' . 'Kelemahannya adalahh: ' . $harimau2->get_defencePower() . '<br>' . '<br>';

$hewan3 = new Hewan('Harimau', 7, '8', 'Lari Cepat');
$hewan3->set_atraksi('Singa', 'Berlari sangat cepat');
echo $hewan3->get_atraksi();
