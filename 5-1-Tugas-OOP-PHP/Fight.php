<?php

class Fight
{
  private $attackPower;
  private $defencePower;

  public function __construct($attackPower, $defencePower)
  {
    $this->attackPower = $attackPower;
    $this->defencePower = $defencePower;
  }

  public function set_attackPower($attackPower)
  {
    $this->attackPower = $attackPower;
  }

  public function get_attackPower()
  {
    return $this->attackPower;
  }

  public function set_defencePower($defencePower)
  {
    $this->defencePower = $defencePower;
  }

  public function get_defencePower()
  {
    return $this->defencePower;
  }
}
