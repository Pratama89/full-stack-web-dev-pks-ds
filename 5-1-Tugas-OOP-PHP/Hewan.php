<?php
class Hewan
{
  protected $nama;
  protected $darah = 50;
  protected $jumlahKaki;
  protected $keahlian;

  public function __construct($nama, $darah, $jumlahKaki, $keahlian)
  {
    $this->nama = $nama;
    $this->darah = $darah;
    $this->jumlahKaki = $jumlahKaki;
    $this->keahlian = $keahlian;
  }

  public function set_nama($nama)
  {
    $this->nama = $nama;
  }

  public function get_nama()
  {
    return $this->nama;
  }

  public function set_darah($darah)
  {
    $this->darah = $darah;
  }

  public function get_darah()
  {
    return $this->darah;
  }

  public function set_jumlahKaki($jumlahKaki)
  {
    $this->jumlahKaki = $jumlahKaki;
  }

  public function get_jumlahKaki()
  {
    return $this->jumlahKaki;
  }

  public function set_keahlian($keahlian)
  {
    $this->keahlian = $keahlian;
  }

  public function get_keahlian()
  {
    return $this->keahlian;
  }

  public function set_atraksi($nama, $keahlian)
  {
    $this->nama = $nama;
    $this->keahlian = $keahlian;
  }

  public function get_atraksi()
  {
    return 'Nama Hewan: ' . $this->nama;
    return 'Keahlian: ' . $this->keahlian;
  }
}
